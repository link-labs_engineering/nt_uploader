package main

import (
	"flag"
	"io/ioutil"
	"log"
	"os"
	"sync"
	"time"

	"github.com/chrizzzzz/go-xmodem/xmodem"
	"github.com/tarm/serial"
)

var errorLog, infoLog, debugLog *log.Logger
var debug bool

func main() {
	verbose := flag.Bool("verbose", false, "Whether to show verbose/debug log or not.")
	firmware := flag.String("firmware", "nil", "Path to the firmware.")
	destructive := flag.Bool("destructive", false, "Wheneter to overwrite the bootloader or not.")
	flag.Parse()
	devices := flag.Args()

	if *verbose {
		debugLog = log.New(os.Stdout, "DEBUG:", log.Ltime)

		debugLog.Println("Arguments:")
		debugLog.Println("\tFirmware Path:", *firmware)
		debugLog.Println("\tVerbose:", *verbose)
		debugLog.Println("\tDestructive:", *destructive)
		debugLog.Println("\tDevices:", devices)

		debug = true
	}

	errorLog = log.New(os.Stdout, "ERROR:", log.Ltime)
	infoLog = log.New(os.Stdout, "INFO:", log.Ltime)

	var wg sync.WaitGroup
	if len(devices) > 0 && *firmware != "nil" {
		for _, element := range devices {
			wg.Add(1)
			go uploadFirmware(element, *firmware, *destructive, &wg)
		}
		wg.Wait()
	} else {
		errorLog.Println("Must have device and firmware arguments to run...")
		infoLog.Println("You must enumerate all of the devices you want to communicate")
		infoLog.Println("with after you specify the firmware and if you want verbose or not")
		infoLog.Println("ex: 'EFM32_bootloader -firmware nt.bin /dev/ttyUSB2 /dev/ttyUSB5''")
		infoLog.Println("Program Usage:")
		flag.PrintDefaults()
	}
}

//------------------------------------------------------------------------------
// Purpose: Uploads the firmware to the devices via xmodem.
//
// Param dev_path: The device path to commmunicate on.
// Param firmware_path: The location on disk of the firmware that is to be
//                      installed.
// Param destructive: To overwrite the bootloader or not.
// Param wg : The waitgroup that is needed to sync the goroutines together with
//            all the other instances of this function.
//------------------------------------------------------------------------------
func uploadFirmware(devPath, firmwarePath string, destructive bool, wg *sync.WaitGroup) {
	logDebug("Reading binary file")
	data, err := ioutil.ReadFile(firmwarePath)
	check(err)

	infoLog.Println("Opening", devPath)
	config := &serial.Config{Name: devPath, Baud: 115200, ReadTimeout: time.Second * 5}

	logDebug("Opening serial port")
	port, err := serial.OpenPort(config)
	check(err)

	if destructive {
		logDebug("Sending destructive xmodem request to serial")
		_, err = port.Write([]byte("d"))
	} else {
		logDebug("Sending xmodem request to serial")
		_, err = port.Write([]byte("u"))
	}
	check(err)

	logDebug("Done sending xmodem request to serial")

	startTime := time.Now()
	infoLog.Println("Starting XMODEM transfer for", devPath)
	check(xmodem.ModemSend(port, data))
	infoLog.Println("Finished XMODEM transfer for", devPath, "in", time.Since(startTime))
	wg.Done()
}

//------------------------------------------------------------------------------
// Purpose: To handle the debug log and only publish the log when the user
// specifies a need for it.
//
// Param text: The text that should output
//------------------------------------------------------------------------------
func logDebug(text string) {
	if debug {
		debugLog.Println(text)
	}
}

//------------------------------------------------------------------------------
// Purpose: Quick error checking to fix the excess amount of error checking we
// need to do
//
// Param err: The error we are checking
//------------------------------------------------------------------------------
func check(err error) {
	if err != nil {
		panic(err)
	}
}
